%global debug_package %{nil}

Name:           properties-cpp
Version:        0.0.1+14.04.20140220
Release:        0
Summary:        C++11 library providing properties/signal
License:        LGPL-3.0
Group:          System/Libraries
Url:            https://launchpad.net/properties-cpp
Source:         http://archive.ubuntu.com/ubuntu/pool/main/p/%{name}/%{name}_%{version}.orig.tar.gz
# PATCH-FIX-OPENSUSE properties-cpp-disable-tests.patch
Patch0:         %{name}-disable-tests.patch
BuildRequires:  cmake
BuildRequires:  gcc-c++
BuildRequires:  pkgconfig

%description
A very simple convenience library for handling properties and
signals in C++11.


%package devel
Summary:        Header files for properties-cpp
Group:          Development/Libraries/C and C++

%description devel
A very simple convenience library for handling properties and
signals in C++11.

This package provides development headers for properties-cpp.

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1

%build
%cmake
make %{?_smp_mflags}

%install
%if %{defined cmake_install}
%cmake_install
%else
%make_install
%endif

%files devel
%defattr(-,root,root)
%doc COPYING README.md
%{_includedir}/core/
%{_libdir}/pkgconfig/%{name}.pc



